'use strict';
'require rpc';
'require view';
'require poll';
'require ui';

var callQMIOverview = rpc.declare({
	'object': 'qmi',
	'method': 'overview'
});

var callHwInfo = rpc.declare({
	'object': 'qmi',
	'method': 'hwinfo'
});


return view.extend({
	load: function() {
		return Promise.all([
			callQMIOverview(),
			callHwInfo()
		]);
	},
	renderOverviewTabTable(data) {
		var celloverview = data[0];
		var hwinfo = data[1];		
		//var cellstatus = E('dev', { 'class': 'network-status-table' });
		const fields = new Map();
		if ("state" in celloverview) {
			fields.set(_("Status"),celloverview["state"]);
		}
		if ("network_name" in celloverview) {
			fields.set(_("Network"),celloverview["network_name"]);
		}
		if ("roaming_status" in celloverview) {
			let roam_status = celloverview["roaming_status"];
			if (roam_status == true) {
				fields.set(_("Roaming Status"), "Yes");
			} else {
				fields.set(_("Roaming Status"), "Home Network");
			}
		}
		
		if ("mode" in celloverview) {
			let mode = celloverview["mode"];
			fields.set(_("Mode"), mode);
			if (mode == "5G NSA / EN-DC") {
				let endc_status = ("nr_rsrq" in celloverview) ? _("Yes") : _("No");
				fields.set(_("EN-DC Active"), endc_status);
			}
		}
		
		if ("bars" in celloverview) {
			const bars = celloverview["bars"];
			var icon;
			switch(bars) {
				case 5:
				icon = L.resource("icons/signal-75-100.png");
				break;
				case 4:
				icon = L.resource("icons/signal-50-75.png");
				break;
				case 3:
				icon = L.resource("icons/signal-25-50.png");
				break;
				case 2:
				icon = L.resource("icons/signal-0-25.png");
				break;
				default:
				icon = L.resource("icons'signal-0.png");
			}
			var signal = ("lte_rssi" in celloverview) ? celloverview["lte_rssi"] + " dBm " : "";
			var signal_span = E('span', [E('span', [signal]), E('img', {'src': icon})]);
			fields.set(_("Signal (RSSI)"), signal_span);
		}
		if ("lte_rsrp" in celloverview) {
			fields.set(_("LTE RSRP"), celloverview["lte_rsrp"] + " dBm");
		}
		if ("lte_rsrq" in celloverview) {
			fields.set(_("LTE RSRQ"), celloverview["lte_rsrq"] + " dB");
		}
		if ("lte_snr" in celloverview) {
			fields.set(_("LTE SNR"), celloverview["lte_snr"] + " dB");
		}
		if ("nr_rsrp" in celloverview) {
			fields.set(_("5G RSRP"), celloverview["nr_rsrp"] + " dBm");
		}
		if ("nr_rsrq" in celloverview) {
			fields.set(_("5G RSRQ"), celloverview["nr_rsrq"] + " dB");
		}
		if ("nr_snr" in celloverview) {
			fields.set(_("5G SNR"), celloverview["nr_snr"] + " dB");
		}
		fields.set(_("IMEI"),hwinfo["imei"]);
		fields.set(_("Active SIM Slot"), celloverview["active_sim_slot"]);
		fields.set(_("Active SIM IMSI"), celloverview["active_sim_imsi"]);
		fields.set(_("Active SIM ICCID"), celloverview["active_sim_iccid"]);
		fields.set(_("Other SIM Slot"), celloverview["other_sim_slot"]);
		fields.set(_("Other SIM Status"), celloverview["other_sim_status"]);
		fields.set(_("Other SIM ICCID"), celloverview["other_sim_iccid"]);
		
		var table = E('table', {'id': 'modem-overview', 'class': 'table' });

		for (const [key,value] of fields) {
			if (value == null || value == undefined)
			continue;
			table.appendChild(E('tr', { 'class': 'tr' }, [
				E('td', { 'class': 'td left', 'width': '33%' }, [ key ]),
				E('td', { 'class': 'td left' }, [ value ])
			]));
		}
		return table;
	},
	render: function(data) {
		var overviewtable = this.renderOverviewTabTable(data);
		var advTabPlaceholder = E('div',[E('h2',_("Placeholder"))]);
		var tabholder = E('div', {}, [
			E('div', {'data-tab': 'overview', 'data-tab-title': 'Overview', 'data-tab-active': 'true'}, [overviewtable]),
			//E('div', {'data-tab': 'advanced', 'data-tab-title': 'Advanced'}, [advTabPlaceholder])
		]);
		var header = E('h2', {}, _("Cellular Modem"));
		var pane = E('div', {}, [header, tabholder]);

		ui.tabs.initTabGroup(tabholder.childNodes);
		this.pollModemOverview();
		return pane;
	},
	pollModemOverview: function() {
		poll.add(L.bind(function() {
			return Promise.all([
				callQMIOverview(),
				callHwInfo()
			]).then(this.renderOverviewTabTable)
			.then(function(table) {
				var existingTable = document.getElementById("modem-overview");
				existingTable.innerHTML = table.innerHTML;
			});
		}, this));
	}
});