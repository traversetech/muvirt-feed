#!/usr/bin/env lua

require "pl"
jsonc = require "luci.jsonc"

local combustignite = {}

function combustignite.create_hashed_passwd(passwd)
    if (passwd == nil) then
        return nil
    end
    -- Check if password is already in crypt format, pass through if so
    if (passwd:find('%$%w+%$') ~= nil) then
        return passwd
    end
    local output_temp_path = path.tmpname()
    -- Call openssl passwd -6 here
    local hashed_password = ""
    local passwd_pipe = io.popen("openssl passwd -6 -stdin -noverify > " .. output_temp_path,"w")
    passwd_pipe:write(passwd)
    passwd_pipe:close()
    local password_temp_file = io.open(output_temp_path)
    -- Use iterator to remove the unwanted newline
    for line in password_temp_file:lines() do
        hashed_password = line
        break
    end
    password_temp_file:close()
    os.remove(output_temp_path)
    return hashed_password
end

function combustignite.create_ignition_config(vmtable)
    local root_table = {}
    local vertable = {version="3.2.0"}
    root_table["ignition"] = vertable
    
    local users_table = {}
    local root_user_table = {name="root"}

    if (vmtable["userpw"] ~= nil) then
        root_user_table["passwordHash"] = combustignite.create_hashed_passwd(vmtable["userpw"])
    end
    if (vmtable["sshkey"] ~= nil) then
        sshkeys = vmtable["sshkey"]
        ssh_authorized_keys = {}
        -- Try to tolerate SSH keys being specified
        -- as a list and as a bunch of strings
        if (type(sshkeys) == "table") then
            for k, v in pairs(sshkeys) do
                table.insert(ssh_authorized_keys, v)
            end
        else
            for line in string.gmatch(sshkeys, "[^\n]+") do
                table.insert(ssh_authorized_keys, line)
            end
        end
        root_user_table["sshAuthorizedKeys"] = ssh_authorized_keys
    end
    users_table[1] = root_user_table
    root_table["passwd"] = {users=users_table}
    return root_table
end

function combustignite.do_combustion_env_subst(script_file, template_vars)
    local gg_template = file.read(script_file)
    local Template = require ('pl.text').Template
    local script_templated = Template(gg_template)
    local script_substituted = script_templated:substitute(template_vars)
    script_substituted = script_substituted:gsub("%%%%{","${")
    return script_substituted
end

function combustignite.do_combustion_env_subst_from_vmtable(script_file, vmtable)
    local configuration_keys = {}
    if (vmtable == nil) then
        return nil
    end
    local uci_config_data = vmtable["config_data"]
    if (uci_config_data == nil) then
        return nil
    end

    for idx, string_val in ipairs(uci_config_data) do
        config_key, ch, config_value = stringx.partition(string_val, "=")
        if (config_key ~= nil) and (config_value ~= nil) then
            configuration_keys[config_key] = config_value
        end
    end
    return combustignite.do_combustion_env_subst(script_file, configuration_keys)
end

function combustignite.create_drive(components, create_fs)
    local do_create_filesystem = create_fs or true
    local temp_folder = path.tmpname()
    file.delete(temp_folder)
    path.mkdir(temp_folder)
    if (components["ignition"] ~= nil) then
        local ignition_json = jsonc.stringify(components["ignition"])
        local ignition_folder_path = path.join(temp_folder,"ignition")
        path.mkdir(ignition_folder_path)
        local ignition_config_ign_path = path.join(ignition_folder_path,"config.ign")
        file.write(ignition_config_ign_path, ignition_json)
    end
    if (components["combustion_script"] ~= nil) then
        local combustion_folder_path = path.join(temp_folder, "combustion")
        path.mkdir(combustion_folder_path)
        local combustion_script_path = path.join(combustion_folder_path, "script")
        file.write(combustion_script_path, components["combustion_script"])
    end
    if (do_create_filesystem) then
        local temp_image_path = path.tmpname()
        file.delete(temp_image_path)
        local create_success = utils.executeex("mkfs.ext4 -L ignition -d " .. temp_folder .. " " .. temp_image_path .. " 4096")
        if (create_success) then
            dir.rmtree(temp_folder)
            return temp_image_path
        end
    end
    return temp_folder
end

return combustignite
