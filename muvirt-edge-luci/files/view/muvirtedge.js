'use strict';
'require ui';
'require rpc';
'require uci';
'require form';
'require network';
'require poll';
'require tools.widgets as widgets';

var callCreateGreengrass = rpc.declare({
	object: "muvirt.microos",
	method: "setup_greengrass",
	params: ["vmid", "aws_region", "aws_access", "aws_secret",
		"greengrass_thing_name", "greengrass_thing_group", "greengrass_version",
		"greengrass_thing_policy", "greengrass_tes_role_name", "greengrass_tes_alias_role_name",
		"cpus", "memory", "rootpw", "sshkey", "image"]
});

const callGetApplianceStoreList = rpc.declare({
	object: "appliancestore",
	method: "getlist",
	params: []
});

var greengrassFormData = {
	greengrass: {
		vmname: null
	}
};


return L.view.extend({
	showApplianceStoreListNotification: function() {
		let curLocation = L.location();
		let appstoreLocation = curLocation.replace("edge","appstore");
		const applianceStoreLink = E("a",{"href": appstoreLocation},_("Go to appliance store"))
		const setupButton = E("button",[applianceStoreLink]);

		const setupButtonDiv = E("div", [setupButton]);
		const alertDiv = E("div", [
		  E("p",
			[
			  E("span",
				_("Update the appliance store lists in order to use automated edge setup"))
			]),
		  setupButtonDiv
		]);
		L.ui.addNotification(_("Update appliance store"), alertDiv);
	},
	checkApplianceStoreListReturn: function(listdata) {
		const numStores = Object.keys(listdata).length;
		if (numStores == 0) {
			this.showApplianceStoreListNotification();
		}
	},
	render: function () {
		const container_div = E("div");
		container_div.appendChild(E("h2", ["Edge Computing Setup"]));

		const greengrass_div = E("div");

		const greengrass_form = this.renderGreengrassForm();
		greengrass_form.render().then(function (node) {
			greengrass_div.appendChild(node);
			let placeholderNode = node.querySelector("table");
			if (placeholderNode != undefined) {
				placeholderNode.parentNode.removeChild(placeholderNode);
			}
		});

		container_div.appendChild(greengrass_div);

		const checkApplianceStoreReturnedFunc = L.bind(this.checkApplianceStoreListReturn,this);
		return callGetApplianceStoreList().
			then(checkApplianceStoreReturnedFunc).
			then(function() {
				return container_div;
			});
	},
	renderGreengrassForm: function () {
		let m = new form.JSONMap(greengrassFormData, _("AWS Greengrass"));

		let s = m.section(form.GridSection, 'greengrass', _('Greengrass client'));
		s.anonymous = true;
		s.addremove = true;
		s.addbtntitle = _('Deploy new Greengrass instance');
		s.handleModalSave = L.bind(function (modalMap, ev) {
			console.log("handleModalSave");
			modalMap.save();
			//const data = modalMap.data.data;
			const newGreengrassData = greengrassFormData["greengrass"];
			const vmname = newGreengrassData["vmname"];
			const awsRegion = newGreengrassData["aws_region"];
			const awsAccess = newGreengrassData["aws_access"];
			const awsSecret = newGreengrassData["aws_secret"];
			const awsGGThingName = newGreengrassData["gg_thing_name"];
			const awsGGThingGroup = newGreengrassData["gg_thing_group"];
			const vmCPUs = newGreengrassData["cpus"];
			const vmMemory = newGreengrassData["memory"];
			const vmRootPw = newGreengrassData["rootpw"];
			const vmSSHKey = newGreengrassData["sshkey"];
			const gg_thing_policy = newGreengrassData["gg_thing_policy"];
			const gg_token_exchange_role = newGreengrassData["gg_token_exchange_role"];
			const gg_tes_alias = newGreengrassData["gg_token_exchange_role_alias"];
			const gg_version = newGreengrassData["gg_version"];
			const vmImage = newGreengrassData["image"];

			callCreateGreengrass(vmname, awsRegion, awsAccess, awsSecret,
				awsGGThingName, awsGGThingGroup, gg_version,
				gg_thing_policy, gg_token_exchange_role, gg_tes_alias,
				vmCPUs,
				vmMemory,
				vmRootPw,
				vmSSHKey,vmImage).then(L.bind(this.handle_greengrass_call_return), this);

		}, this);

		s.addModalOptions = function (s) {
			const compute_description = _("More compute/VM options can be configured in the next step");

			// Virtual machine options section
			let o = s.option(form.SectionValue, '_vmconfig', form.NamedSection,
				"greengrass", "greengrass", _('Compute Instance Configuration'),
				compute_description);
			o.addremove = true;
			let ss = o.subsection;

			o = ss.option(form.Value, 'vmname', _('Virtual Machine Name'));
			o.datatype = 'uciname';
			o.default = 'greengrass';

			o = ss.option(form.Value, 'cpus', _('Number of Virtual CPUs'));
			o.datatype = 'uinteger';
			o.default = '2';
			o = ss.option(form.Value, 'memory', _('Amount of RAM'));
			o.datatype = 'uinteger';
			o.default = '2048';
			o = ss.option(form.Value, 'rootpw',
				_('Greengrass VM root password'),
				_('If blank, you will only be able to access the VM via SSH keys'));
			o.password = true;

			// Greengrass specific options
			o = ss.option(form.DynamicList, 'sshkey',
				_('Greengrass VM SSH keys'),
				_('If blank, you will only be able to access the VM via the console with the root password'));

			let o2 = s.option(form.SectionValue, '_ggconfig', form.NamedSection, 
					  "greengrass", "greengrass", _('Greengrass client configuration'));
			let s2 = o2.subsection;
			s2.tab('general', _('General Settings'));
			s2.tab('advanced', _('Advanced Settings'));

			o = s2.taboption('general', form.Value, 'aws_access', _('AWS Access Key'));
			o.optional = false;
			o = s2.taboption('general', form.Value, 'aws_secret', _('AWS Secret Key'));
			o.password = true;
			o.optional = false;
			o = s2.taboption('general', form.Value, 'aws_region', _('AWS Region'));
			o.default = 'us-east-1';
			o.optional = false;
			o = s2.taboption('general', form.Value, 'gg_thing_name', _('Greengrass Thing Name'));
			o.optional = false;
			o = s2.taboption('general', form.Value, 'gg_thing_group', _('Greengrass Thing Group'));
			o.optional = false;
			o = s2.taboption('advanced', form.Value, 'gg_thing_policy', _('Greengrass Thing Policy'));
			o.placeholder = "GreengrassV2IoTThingPolicy";
			o.optional = true;
			o = s2.taboption('advanced', form.Value,
				'gg_token_exchange_role',
				_('Greengrass Token Exchange Role Name'));
			o.optional = true;
			o.placeholder = "GreengrassV2TokenExchangeRole";
			o = s2.taboption('advanced', form.Value, 
				'gg_token_exchange_role_alias', 
				_('Greengrass Token Exchange Role Alias Name'));
			o.optional = true;
			o.placeholder = "GreengrassCoreTokenExchangeRoleAlias";
			o = s2.taboption('advanced', form.Value, 'gg_version', _('Greengrass Core Version'), _('If blank, the latest available Greengrass core version will be installed'));
			o.placeholder = 'nucleus-latest';
			o.optional = true;
			o = s2.taboption('advanced', form.Value, 'image', _('Operating System Appliance'),
				_('Default is &quot;opensuse-leap-micro-5.5&quot; Tumbleweed (rolling) can be specified with &quot;opensuse-tumbleweed-microos-vm&quot;'));
			o.placeholder = 'opensuse-leap-micro-5.5';
			o.optional = true;
		};

		return m;
	},
	handle_greengrass_call_return: function (return_data) {
		console.log("handle_greengrass_call_return");
		if (typeof(return_data) == "number") {
			window.alert(_("Create call failed, error code: ") + return_data);
		} else if ("error" in return_data) {
			window.alert(_("Unable to create Greengrass client: ") + return_data["error"]);
		} else {
			const new_vm_name = return_data["vmname"];
			L.ui.hideModal();
			var editPage = L.url("admin/muvirt/status") + "#" + new_vm_name;
			window.location.href = editPage;
		}

	},

	handleSaveApply: null,
	handleReset: null
});
