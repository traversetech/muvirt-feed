#!/bin/sh

get_storage_device() {
	device_portion=$(echo -n "${1}" | awk -F ',' '{print $1}')
	if [[ $device_portion == *"UUID="* ]]; then
			device_portion=$(blkid -U "${device_portion:5}")      
	fi
	echo -n "${device_portion}"
}

get_storage_device_arguments() {
	numargs=$(echo -n "${1}" | awk -F ',' '{print NF}')
	additional_args=""
	for i in $(seq 2 "${numargs}"); do
		token=$(echo -n "${1}" | awk -F ',' "{print \$${i}}")
		key=$(echo -n "${token}" | awk -F '=' '{print $1}')
		value=$(echo -n "${token}" | awk -F '=' '{print $2}')

		if [[ "${key}" = "serial" ]]; then
			additional_args=",serial=${value}"
		fi
	done
	echo "${additional_args}"
}

is_device_a_bridge() {
	device_name="${1}"
	device_bridge_status=$(lua -e "muvirt = require 'muvirt'; print(muvirt.is_device_bridge('${device_name}'))")
	[ "${device_bridge_status}" = "true" ] && return 0
	return 1
}

do_hugetlb_setup() {
	lua -e "muvirt = require 'muvirt'; muvirt.do_hugetlb_setup()" > /dev/null
}
